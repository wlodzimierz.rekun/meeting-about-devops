# meeting-about-devops

## WYNIKI


### Główne obszary zadań do wykonania przez DevOps z punktu widzenia Teamliderów:

1.	Przejście z Jenkins do Gitlab Runner z dostępem do sieci lokalnej (wymagane projekty – PCEngines i Mezrit)
2.	Aktualizacja kontenerów Docker na wszystkich projektach
3.	Maksymalna automatyzacja projektów PCEngines I Yocto 


>> Proszę o dodanie drobiazgów do zadań.
___________________



Project meeting
===============

Meeting information
-------------------

* **Meeting Date/Time:** DD-MM-RRRR, HH:MM
* **Duration:** HH:MM
* **Meeting purpose:** meeting_purpose
* **Meeting location:** meeting_location
* **Note taker:** note_taker

Attendees
---------

* 1st person
* 2nd person
* …

Agenda items
------------

| Item    | Description |
|:-------:|:-----------:|
| item #1 | description |
| item #2 | description |
| item #3 | description |

Discussion items for next meeting
---------------------------------

| Item    | Who | Notes |
|:-------:|:---:|:-----:|
| item #1 | who | notes |
| item #2 | who | notes |
| item #3 | who | notes |

Action items
------------

| JIRA task | Item    | Responsible | Notes |Due Date  |
|:---------:|:-------:|:-----------:|:-----:|:--------:|
| task_link | item #1 | who         | notes | due_date |
| task_link | item #2 | who         | notes | due_date |
| task_link | item #3 | who         | notes | due_date |

