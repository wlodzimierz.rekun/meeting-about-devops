# meeting-about-devops

## Co zakłada DevOps:
* automatyzacja – pozbieganie manualnych działań 
* uszczuplać proces developmentu
* skrócić czas I obniżyć koszty procesu wytwarzania oprogramowania
* zminimalizowanie zaangażowania człowieka w proces tworzenia I uruchamiania aplikacji
* zmniejszenie kosztów operacyjnych dzięki optymalizacji usług i automatycznemu skalowaniu na żądanie
* deployment lub migracja aplikacji do konteneru (docker lub inny)
* wyszukiwanie, testowanie oraz wdrażanie narzędzi, procesów i rozwiązań ułatwiających, ulepszających i przyspieszających pracę zespołu wytwórczego,
* planowanie, rozbudowa i administracja systemami CI/CD (wirtualizacja, konteneryzacja)


___________________

### Celem stosowania CI (continuous integration) jest:
* osiągnięcie spójnego i zautomatyzowanego sposobu budowania i testowania aplikacji.

### Kluczową cechą CD (continuous delivery) jest: 
* zadbanie o automatyzacje procesu dostarczania zmian dla wszystkich wykorzystywanych środowisk.

 CD automatyzuje proces wdrażania aplikacji i wprowadzanych zmian w kodzie do przygotowanej infrastruktury serwerowej.
 

 CI oraz CD wymagają ciągłego przeprowadzania testów (Continous Testing), ponieważ celem jest dostarczenie wysokiej jakości aplikacji i kodu do użytkownika końcowego

>> Praca z wykorzystaniem metodyki DevOps to ciągła integracja, ciągłe wdrażanie czy ciągłe dostarczanie oprogramowania

________________

Wszystko to przekłada się na zmniejszenie kosztów i czasu dostarczenia produktu, krótszy czas i koszt testowania przy jednoczesnym zwiększeniu jego zasięgu, jak i możliwości wykorzystania środowiska. Devops pozwala na minimalizowanie przestojów, problemów w czasie wdrażania oraz roll-back aplikacji. Zwiększenie zdolności do reprodukcji i naprawy błędów oraz minimalizuje średni czas przywrócenia (MTTR)



